package com.yb7s.springboot.shiro.controller;

import com.yb7s.springboot.shiro.common.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HelloController {

    @RequestMapping("/hello")
    public Map greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        Map result = new HashMap<>();
        try {
            result.putAll(ResponseEntity.responseSuccess("返回 hello"));;
        }catch (Exception e) {
            result.putAll(ResponseEntity.responseError());;
        }
        return result;
    }
    @RequestMapping("/login")
    public ModelAndView login() {
        ModelAndView view = new ModelAndView("/login");
//        view.setViewName("/login");
        return view;
    }
}

package com.yb7s.springboot.generator;


import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author ybzhu
 * @since 2018-09-12
 */
public class MysqlGenerator {

    public static void main(String[] args) {
        List<String> strings = Arrays.asList(
                "sys_user","sys_role","sys_permission","sys_role_permission","sys_user_role"
        );
        //输入表名逗号隔开即可
        Generator((String[]) strings.toArray());
    }

    public static void Generator(String[] tableName) {

        String projectPath = System.getProperty("user.dir");

        //============================== 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(projectPath + "/springboot-generator/src/main/java")
                .setActiveRecord(true)// 是否支持 AR
                .setAuthor("ybzhu") //设置作者名字
                .setFileOverride(true) //文件覆盖(全新文件)
                .setIdType(IdType.AUTO)//主键策略
                .setBaseResultMap(true) //SQL 映射文件
                .setBaseColumnList(true)//SQL 片段
                .setSwagger2(true)
                .setOpen(false);


        //============================== 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MARIADB)
                .setUrl("jdbc:mariadb://localhost:3307/springboot-shiro-side")
                .setDriverName("org.mariadb.jdbc.Driver")
                .setUsername("root")
                //.setSchemaName("public")
                .setPassword("root");
        //==============================包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.yb7s.springboot.generator")//配置父包路径
                .setModuleName("base")//配置业务包路径
                .setMapper("mapper")
                .setXml("mapper")
                .setEntity("entity")
                .setService("service")
                .setController("controller");
        //.setServiceImpl("service.impl"); 会自动生成 impl，可以不设定

        //============================== 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        List<FileOutConfig> focList = new ArrayList<>();
        focList.add(new FileOutConfig("/templates/mapper.xml.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return projectPath + "/springboot-generator/src/main/java/" + pc.getParent().replace(".", "/") + "/mapper/"
                        + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);


        //============================== 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel)//设置命名规则  underline_to_camel 底线变驼峰
                .setColumnNaming(NamingStrategy.underline_to_camel)//设置设置列命名  underline_to_camel 底线变驼峰
                //.setSuperEntityClass("com.maoxs.pojo")//设置继承类
                //.setSuperControllerClass("com.maoxs.controller")//设置继承类
                .setEntityLombokModel(true)//是否加入lombok
                .setInclude(tableName)//设置表名
                //.setSuperEntityColumns("id") //设置超级超级列
                .setControllerMappingHyphenStyle(true)//设置controller映射联字符
                .setTablePrefix(pc.getModuleName() + "_");//表的前缀

        //============================== 生成配置
        AutoGenerator mpg = new AutoGenerator();
        //
        mpg.setCfg(cfg)
                .setTemplate(new TemplateConfig().setXml(null))
                .setGlobalConfig(gc)
                .setDataSource(dsc)
                .setPackageInfo(pc)
                .setStrategy(strategy)
                // 选择 freemarker 引擎需要指定如下加，注意 pom 依赖必须有！
                .setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }


}

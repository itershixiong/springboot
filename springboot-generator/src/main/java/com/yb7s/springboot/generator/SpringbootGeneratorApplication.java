package com.yb7s.springboot.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootGeneratorApplication.class, args);
    }

}

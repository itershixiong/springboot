package com.yb7s.springboot.jdbc.service;

import com.yb7s.springboot.jdbc.pojo.User;

import java.util.List;

/**
 * @Date: 2019/3/3 12:48
 * @Auther: ybzhu
 * @Description：
 */
public interface UserService {
    int add(User user);

    int update(User user);

    int delete(int id);

    User findUserById(int id);

    List<User> findUserList();
}

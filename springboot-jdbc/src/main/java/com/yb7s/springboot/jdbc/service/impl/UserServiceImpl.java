package com.yb7s.springboot.jdbc.service.impl;

import com.yb7s.springboot.jdbc.dao.UserDao;
import com.yb7s.springboot.jdbc.pojo.User;
import com.yb7s.springboot.jdbc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Date: 2019/3/3 12:48
 * @Auther: ybzhu
 * @Description：
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    /**
     * 添加 用户信息
     * @param user
     * @return
     */
    @Override
    public int add(User user) {
        return userDao.add(user);
    }

    /**
     * 修改用户信息
     * @param user
     * @return
     */
    @Override
    public int update(User user) {
        return userDao.update(user);
    }

    /**
     * 删除一个用户信息
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        return userDao.delete(id);
    }

    /**
     * 查询一个用户信息
     * @param id
     * @return
     */
    @Override
    public User findUserById(int id) {
        return userDao.findUserById(id);
    }

    /**
     * 查询所有用户列表
     * @return
     */
    @Override
    public List<User> findUserList() {
        return userDao.findUserList();
    }
}

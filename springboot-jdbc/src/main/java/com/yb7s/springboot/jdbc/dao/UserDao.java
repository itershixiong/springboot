package com.yb7s.springboot.jdbc.dao;


import com.yb7s.springboot.jdbc.pojo.User;

import java.util.List;

/**
 * @Date: 2019/3/3 12:49
 * @Auther: ybzhu
 * @Description：
 */
public interface UserDao {
    int add(User user);

    int update(User user);

    int delete(int id);

    User findUserById(int id);

    List<User> findUserList();
}

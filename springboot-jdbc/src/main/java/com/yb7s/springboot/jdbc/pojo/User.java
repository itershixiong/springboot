package com.yb7s.springboot.jdbc.pojo;

/**
 * @Date: 2019/3/3 12:47
 * @Auther: ybzhu
 * @Description：
 */
public class User {
    private int id ;
    private String name ;
    private double money;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}

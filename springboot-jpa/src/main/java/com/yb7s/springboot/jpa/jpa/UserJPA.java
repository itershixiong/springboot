package com.yb7s.springboot.jpa.jpa;

import com.yb7s.springboot.jpa.pojo.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;

/**
 * @Date: 2019/3/2 23:55
 * @Auther: 11066
 * @Description：
 */
public interface UserJPA extends JpaRepository<UserEntity,Long>, JpaSpecificationExecutor<UserEntity>, Serializable {
}

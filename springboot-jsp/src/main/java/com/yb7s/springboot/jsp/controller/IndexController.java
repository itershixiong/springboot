package com.yb7s.springboot.jsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Date: 2019/3/3 21:58
 * @Auther: ybzhu
 * @Description：
 */
@Controller
public class IndexController {
    /**
     * 根目录转发到首页
     * @return
     */
    @RequestMapping(value = "/")
    public String defaults()
    {
        return "redirect:/index";
    }

    /**
     * 跳转 首页
     * @return
     */
    @RequestMapping(value = "/index")
    public String index()
    {
        return "index";
    }
}

package com.yb7s.springboot.mybatis.service.Impl;

import com.yb7s.springboot.mybatis.entity.Customer;
import com.yb7s.springboot.mybatis.mapper.CustomerMapper;
import com.yb7s.springboot.mybatis.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Date: 2019/3/15 11:25
 * @Auther: ybzhu
 * @Description：
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerMapper customerMapper;

    /**
     * 验证登录
     * @param customer
     * @return
     */
    @Override
    public Customer login(Customer customer) {
        //开始验证
        Customer c= customerMapper.login(customer);
        return c;
    }

    /**
     * 修改密码
     * @param customer
     * @return
     */
    @Override
    public int updatePassword(Customer customer) {
        // 进行修改
        int flag=customerMapper.updatePassword(customer);
        return flag;
    }

    /**
     * 注册用户
     * @param customer
     * @return
     */
    @Override
    public int regiter(Customer customer) {
        int flag=customerMapper.insert(customer);
        return flag;
    }
}

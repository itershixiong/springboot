package com.yb7s.springboot.mybatis.mapper;

import com.yb7s.springboot.mybatis.entity.Customer;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CustomerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Customer record);

    Customer selectByPrimaryKey(Integer id);

    List<Customer> selectAll();

    int updateByPrimaryKey(Customer record);

    Customer login (Customer customer);

    int updatePassword (Customer customer);
}
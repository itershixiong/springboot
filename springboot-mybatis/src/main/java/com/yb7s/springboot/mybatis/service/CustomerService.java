package com.yb7s.springboot.mybatis.service;

import com.yb7s.springboot.mybatis.entity.Customer;

/**
 * @Date: 2019/3/15 11:25
 * @Auther: ybzhu
 * @Description：
 */
public interface CustomerService {
    Customer login(Customer customer);

    int updatePassword(Customer customer);

    int regiter(Customer customer);
}

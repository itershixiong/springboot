package com.yb7s.springboot.mybatis.controller;

import com.yb7s.springboot.mybatis.entity.Customer;
import com.yb7s.springboot.mybatis.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * @Date: 2019/3/15 11:24
 * @Auther: ybzhu
 * @Description：
 */
@Controller
//窄化请求
@RequestMapping("/customer")
public class CustomerController {

    /**
     * 注入 用户接口
     */
    @Autowired
    private CustomerService customerService;

    /**
     * 跳转首页
     * @return
     */
    @RequestMapping("/show")
    public String show() {
        return "main";
    }

    /**
     * 注册用户
     * @param customer
     * @return
     */
    @RequestMapping("/regist")
    public String regist(Customer customer) {
        //得到传入的信息
        customer.setIs_valid(1);
        customer.setRegist_date(new Date());
        int flag=customerService.regiter(customer);
        if(flag==1){
            System.out.println("注册成功");
        }else if(flag==0){
            System.out.println("注册失败");
        }else {
            System.out.println("其他错误");
        }
        return "main";
    }

    /**
     * 登陆
     * @param customer
     * @param session
     * @return
     */
    @RequestMapping("/login")
    public String login(Customer customer , HttpSession session) {
        //将接收到的账户密码去验证是否存在，存在则 so有数据不为null，不存在则为null
        Customer so = customerService.login(customer);
        if(so!=null) {
            session.setAttribute("so", so);
        }else {
            session.setAttribute("so", null);
        }
        return "main";

    }
    /**
     * 退出
     * @param session
     * @return
     */
    @RequestMapping("/loginout")
    public String sign(HttpSession session) {
        //退出则直接在缓存中删除当前用户信息  so
        session.removeAttribute("so");
        return "main";

    }
    /**
     * 修改密码
     */
    @RequestMapping("/modify")
    public String modify(Customer customer , HttpSession session) {
        //修改密码  得到传入的ID、密码，然后在后台进行修改
        //修改成功后返回类型为0 或 1  0：修改失败，1：修改成功
        int flag = customerService.updatePassword(customer);
        if(flag==1){
            System.out.println("修改成功");
        }else if(flag==0){
            System.out.println("修改失败");
        }else {
            System.out.println("其他错误");
        }
        return "main";
    }
}
